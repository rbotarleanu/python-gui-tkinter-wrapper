from Tkinter import *
import sys

class WindowManager(object):
    """A class that handles the creation of the application window.
       A basic wrapper over TK.
    """

    def button_test_callback(self):
        print "You clicked me!"

    # Create the game window with the given title and dimensions
    def __init__(self, title, width, height, icon = None):
        self.window = Tk()  
        self.canvas = Canvas(self.window)  
        self.title = title
        self.width = width
        self.height = height
        if(icon == None):
            self.icon = 'transparent.ico'
        else:
            self.icon = icon
        # print dir(self.window) best command eva
    
    # change the current window title
    def change_window_title(self, title):
        self.title = title
        self.window.title(title)

    # change the window icon
    def change_icon(self, icon):
        self.window.iconbitmap(default=icon)

    #scroll bar click
    def scrollbar_click(self, count):
        print "You clicked: {}".format(self.listbox.get(ACTIVE))
        # or self.listbox.get(ACTIVE) / self.listbox.get(ANCHOR)

    # puts the window on the screen
    def run(self):
        # set the title
        self.window.title(self.title)
        # set the icon
        self.window.wm_iconbitmap(self.icon)
        # set the application size
        self.window.geometry("{}x{}".format(self.width, self.height))
        # add a menu
        menubar = Menu(self.window)             
        # first column is for the file options
        filemenu = Menu(menubar, tearoff=0)   
        # adds an exit protocol  
        filemenu.add_command(label = "Exit", command = self.close_window)
        # add the file menu to the menubar
        menubar.add_cascade(label="File", menu=filemenu)
        # configure the window
        self.window.config(menu=menubar)
        # add an exit protocol to the X button
        self.window.protocol("WM_DELETE_WINDOW", self.close_window)
        # add a test button
        b = Button(self.window, text="Click me!", command = self.button_test_callback)
        b.pack()
        # add a scrollbar
        self.scrollbar = Scrollbar(self.window)
        self.scrollbar.pack(side = RIGHT, fill = Y)
        self.listbox = Listbox(self.window, yscrollcommand = self.scrollbar.set)
        for i in range(1000):
            self.listbox.insert(END, str(i))
        # bind the button listener
        self.listbox.bind("<Button-1>", self.scrollbar_click)
        # also <Double-Button-1> etc.
        self.listbox.pack(side = LEFT, fill = BOTH)
        self.scrollbar.config(command = self.listbox.yview)

        # start the main loop
        self.running = True
        self.loop_forever()

    # the main loop of the GUI
    def loop_forever(self):
        while self.running:
            self.window.update_idletasks()
            self.window.update()

    # closes the window
    def close_window(self):
        self.window.destroy()
        self.running = False
        sys.exit()      # exit the thread


    # destructor for the window manager
    def __exit__(self, exc_type, exc_value, traceback):
        self.window.destroy()       # destroy the window also
